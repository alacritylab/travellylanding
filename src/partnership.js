module.exports = function(req) {
  return `


  <html>
  <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
  <title>Notify</title>

  <style type="text/css">

  div, p, a, li, td { -webkit-text-size-adjust:none; }

  *{
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  }

  .ReadMsgBody
  {width: 100%; background-color: #ffffff;}
  .ExternalClass
  {width: 100%; background-color: #ffffff;}
  body{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
  html{width: 100%; background-color: #ffffff;}

  @font-face {
      font-family: 'proxima_novalight';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

  @font-face {
      font-family: 'proxima_nova_rgregular'; src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

  @font-face {
      font-family: 'proxima_novasemibold';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

  @font-face {
  	font-family: 'proxima_nova_rgbold';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

  @font-face {
      font-family: 'proxima_novablack';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

  @font-face {font-family: 'proxima_novathin';src: url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

  p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important; }

  .hover:hover {opacity:0.85;filter:alpha(opacity=85);}

  .image77 img {width: 77px; height: auto;}
  .avatar125 img {width: 125px; height: auto;}
  .icon61 img {width: 61px; height: auto;}
  .logo img {width: 75px; height: auto;}
  .icon18 img {width: 18px; height: auto;}

  </style>

  <!-- @media only screen and (max-width: 640px)
  		   {*/
  		   -->
  <style type="text/css"> @media only screen and (max-width: 640px){
  		body{width:auto!important;}
  		table[class=full2] {width: 100%!important; clear: both; }
  		table[class=mobile2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
  		table[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
  		td[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
  		td[class=pad15] {width: 100%!important; padding-left: 15px; padding-right: 15px; clear: both;}

  } </style>
  <!--

  @media only screen and (max-width: 479px)
  		   {
  		   -->
  <style type="text/css"> @media only screen and (max-width: 479px){
  		body{width:auto!important;}
  		table[class=full2] {width: 100%!important; clear: both; }
  		table[class=mobile2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
  		table[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
  		td[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
  		table[class=full] {width: 100%!important; clear: both; }
  		table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
  		table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
  		td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
  		td[class=pad15] {width: 100%!important; padding-left: 15px; padding-right: 15px; clear: both;}
  		.erase {display: none;}

  		}
  } </style>

  </head>
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <!-- Notification 3 -->
  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full"  bgcolor="#303030"style="background-color: rgb(48, 48, 48);">
  	<tr>
  		<td align="center" style="background-image: url('http://travelly.ru/assets/img/mail.png'); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-position: center center; background-repeat: no-repeat; background-color: #303030;"id="not3">


  			<!-- Mobile Wrapper -->
  			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
  				<tr>
  					<td width="100%" align="center">

  						<div class="sortable_inner ui-sortable">
  						<!-- Space -->
  						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
  							<tr>
  								<td width="352" height="30"></td>
  							</tr>
  						</table><!-- End Space -->

  						<!-- Space -->
  						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
  							<tr>
  								<td width="352" height="50"></td>
  							</tr>
  						</table><!-- End Space -->
  						</div>

  						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff"style="border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; -webkit-box-shadow: rgba(68, 68, 68, 0.6) 5px 5px 5px; box-shadow: rgba(68, 68, 68, 0.2) 0px 0px 7px; background-color: rgb(255, 255, 255);">
  							<tr>
  								<td width="352" valign="middle" align="center">


  									<div class="sortable_inner ui-sortable">
  									<!-- Start Top -->
  									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff"style="border-top-left-radius: 5px; border-top-right-radius: 5px; background-color: rgb(255, 255, 255);" object="drag-module-small">
  										<tr>
  											<td width="352" valign="middle" align="center">

  												<!-- Header Text -->
  												<table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
  													<tr>
  														<td width="100%" height="30"></td>
  													</tr>
  												</table>
  											</td>
  										</tr>
  									</table>

  									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
  										<tr>
  											<td width="352" valign="middle" align="center">

  												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
  													<tr>
  														<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(255, 255, 255); line-height: 24px;"class="fullCenter"   cu-identify="element_003275097421335005">
  															<!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]-->July 2017</span></td>
  													</tr>
  												</table>
  											</td>
  										</tr>
  									</table>

  									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
  										<tr>
  											<td width="352" valign="middle" align="center">

  												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
  													<tr>
  														<td width="100%" height="40"></td>
  													</tr>
  												</table>
  											</td>
  										</tr>
  									</table>

  									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
  										<tr>
  											<td width="352" valign="middle" align="center">

  												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
  													<tr>
  														<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 43px; color: #3f4345; line-height: 48px;"class="fullCenter" ><span style="font-family: proxima_novasemibold, Helvetica;"><img src="data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53
  My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5r
  IiB2aWV3Qm94PSIwIDAgMTY2IDUwIj48ZGVmcz48c3R5bGU+LmNscy0xe2ZpbGw6I2ZmZjt9LmNs
  cy0ye21hc2s6dXJsKCNtYXNrKTt9LmNscy0ze21hc2s6dXJsKCNtYXNrLTIpO30uY2xzLTR7Zmls
  bDojZmU4MDI2O30uY2xzLTV7bWFzazp1cmwoI21hc2stMyk7fS5jbHMtNntmaWxsOiNlMDAxM2Y7
  fS5jbHMtN3ttYXNrOnVybCgjbWFzay00KTt9LmNscy04e2ZpbGw6IzVhMjY4ODt9PC9zdHlsZT48
  bWFzayBpZD0ibWFzayIgeD0iMTUyLjQyIiB5PSIyMS4zOCIgd2lkdGg9IjExLjczIiBoZWlnaHQ9
  IjEzLjc5IiBtYXNrVW5pdHM9InVzZXJTcGFjZU9uVXNlIj48ZyBpZD0ibWFzazBfb3V0bGluZSIg
  ZGF0YS1uYW1lPSJtYXNrMCBvdXRsaW5lIj48ZyBpZD0iQ2xpcF8xNCIgZGF0YS1uYW1lPSJDbGlw
  IDE0Ij48cGF0aCBpZD0icGF0aDZfZmlsbCIgZGF0YS1uYW1lPSJwYXRoNiBmaWxsIiBjbGFzcz0i
  Y2xzLTEiIGQ9Ik0xNjQuMTUsMzUuMTdWMjEuMzhIMTUyLjQyVjM1LjE3WiIvPjwvZz48L2c+PC9t
  YXNrPjxtYXNrIGlkPSJtYXNrLTIiIHg9IjAiIHk9IjAuMjUiIHdpZHRoPSIyMi4yNSIgaGVpZ2h0
  PSI0Mi45NyIgbWFza1VuaXRzPSJ1c2VyU3BhY2VPblVzZSI+PGcgaWQ9Im1hc2sxX291dGxpbmUi
  IGRhdGEtbmFtZT0ibWFzazEgb3V0bGluZSI+PGcgaWQ9IkNsaXBfMjYiIGRhdGEtbmFtZT0iQ2xp
  cCAyNiI+PHBhdGggaWQ9InBhdGgxMl9maWxsIiBkYXRhLW5hbWU9InBhdGgxMiBmaWxsIiBjbGFz
  cz0iY2xzLTEiIGQ9Ik0yMi4yNSwzNy43MkExOS44NywxOS44NywwLDAsMCwyMSw0MC4ySDEzLjU4
  QzkuNzksMzEuMzYuODgsMjksLjg4LDE2LjQ2Ljg4LDYuODUsOC4zNS41LDE3LjI3LjVoMGExOCwx
  OCwwLDAsMSw1LC42OVoiLz48L2c+PC9nPjwvbWFzaz48bWFzayBpZD0ibWFzay0zIiB4PSI1Ljk4
  IiB5PSIwIiB3aWR0aD0iMjEuNjEiIGhlaWdodD0iNDAuMiIgbWFza1VuaXRzPSJ1c2VyU3BhY2VP
  blVzZSI+PGcgaWQ9Im1hc2syX291dGxpbmUiIGRhdGEtbmFtZT0ibWFzazIgb3V0bGluZSI+PGcg
  aWQ9IkNsaXBfMjkiIGRhdGEtbmFtZT0iQ2xpcCAyOSI+PHBhdGggaWQ9InBhdGgxNF9maWxsIiBk
  YXRhLW5hbWU9InBhdGgxNCBmaWxsIiBjbGFzcz0iY2xzLTEiIGQ9Ik0yNy41OSwzLjg1QTE3LDE3
  LDAsMCwwLDE3LjI3LjVoMEExNi44NCwxNi44NCwwLDAsMCw2LDQuNjVWMjkuOTFjMi42NiwzLjE4
  LDUuNzUsNiw3LjYsMTAuMjlIMjFjMS42My0zLjgxLDQuMjEtNi40Miw2LjYyLTkuMTVaIi8+PC9n
  PjwvZz48L21hc2s+PG1hc2sgaWQ9Im1hc2stNCIgeD0iMTcuNzYiIHk9IjIuMzEiIHdpZHRoPSIx
  Ni43MiIgaGVpZ2h0PSIzMi4yNSIgbWFza1VuaXRzPSJ1c2VyU3BhY2VPblVzZSI+PGcgaWQ9Im1h
  c2szX291dGxpbmUiIGRhdGEtbmFtZT0ibWFzazMgb3V0bGluZSI+PGcgaWQ9IkNsaXBfMzIiIGRh
  dGEtbmFtZT0iQ2xpcCAzMiI+PHBhdGggaWQ9InBhdGgxNl9maWxsIiBkYXRhLW5hbWU9InBhdGgx
  NiBmaWxsIiBjbGFzcz0iY2xzLTEiIGQ9Ik0yNC41MiwzNC41NmM0LjExLTUsOS4xNS04LjY5LDku
  MTUtMTguMUExNS40LDE1LjQsMCwwLDAsMjUuMTIsMi4zMUgxNy43NlYzNC41NloiLz48L2c+PC9n
  PjwvbWFzaz48L2RlZnM+PHRpdGxlPkFydGJvYXJkIDE8L3RpdGxlPjxnIGlkPSJDYW52YXMiPjxn
  IGlkPSJsb2dvIj48ZyBpZD0iR3JvdXBfMjQiIGRhdGEtbmFtZT0iR3JvdXAgMjQiPjxnIGlkPSJG
  aWxsXzEiIGRhdGEtbmFtZT0iRmlsbCAxIj48cGF0aCBpZD0icGF0aDBfZmlsbCIgZGF0YS1uYW1l
  PSJwYXRoMCBmaWxsIiBkPSJNNDguNjUsMTcuODJWMjEuM0g1MnYyLjU2SDQ4LjY1djZjMCwxLjY3
  LjQ2LDIuNTEsMS43OSwyLjUxYTQuNjgsNC42OCwwLDAsMCwxLjM3LS4xNGwuMDYsMi41OWE3Ljg1
  LDcuODUsMCwwLDEtMi41My4zNiw0LjA1LDQuMDUsMCwwLDEtMy0xLjExLDUuMjcsNS4yNywwLDAs
  MS0xLjExLTMuODJWMjMuODZoLTJWMjEuM2gyVjE4Ljc2WiIvPjwvZz48ZyBpZD0iRmlsbF8zIiBk
  YXRhLW5hbWU9IkZpbGwgMyI+PHBhdGggaWQ9InBhdGgxX2ZpbGwiIGRhdGEtbmFtZT0icGF0aDEg
  ZmlsbCIgZD0iTTU0LjcxLDI1LjY3YzAtMS44NCwwLTMuMTgtLjExLTQuMzdoM2wuMTQsMi41Nmgu
  MDlBNC4xNCw0LjE0LDAsMCwxLDYxLjY2LDIxYTMuNzcsMy43NywwLDAsMSwuODMuMDh2My4yYTUu
  NzYsNS43NiwwLDAsMC0xLjA1LS4wOCwzLjA2LDMuMDYsMCwwLDAtMy4xMywyLjU5LDUuODcsNS44
  NywwLDAsMC0uMDksMXY3aC0zLjVaIi8+PC9nPjxnIGlkPSJGaWxsXzUiIGRhdGEtbmFtZT0iRmls
  bCA1Ij48cGF0aCBpZD0icGF0aDJfZmlsbCIgZGF0YS1uYW1lPSJwYXRoMiBmaWxsIiBkPSJNODEu
  MzMsMjEuM2wyLjI1LDYuODhjLjQsMS4yLjY4LDIuMjguOTQsMy40aC4wOWMuMjYtMS4xMS41Ny0y
  LjE3Ljk0LTMuNGwyLjIyLTYuODhoMy42N0w4Ni4xNCwzNC44N0g4Mi43TDc3LjU1LDIxLjNaIi8+
  PC9nPjxnIGlkPSJGaWxsXzciIGRhdGEtbmFtZT0iRmlsbCA3Ij48cGF0aCBpZD0icGF0aDNfZmls
  bCIgZGF0YS1uYW1lPSJwYXRoMyBmaWxsIiBkPSJNMTI1LjY2LDIxLjNsMi40OCw3LjI3Yy4yOC44
  NC42LDEuODcuOCwyLjYySDEyOWMuMi0uNzUuNDYtMS43OC43NC0yLjY1bDIuMTYtNy4yNGgzLjcz
  bC0zLjQ3LDkuMjVjLTEuOTEsNS4wNy0zLjE5LDcuMzMtNC44MSw4LjcyQTcuNDYsNy40NiwwLDAs
  MSwxMjMuNyw0MWwtLjgtMi44N2E1LjkyLDUuOTIsMCwwLDAsMi0uOTVBNi4wNyw2LjA3LDAsMCww
  LDEyNi44NiwzNWExLjIsMS4yLDAsMCwwLDAtMS4yOGwtNS4xLTEyLjQ1WiIvPjwvZz48ZyBpZD0i
  RmlsbF85IiBkYXRhLW5hbWU9IkZpbGwgOSI+PHBhdGggaWQ9InBhdGg0X2ZpbGwiIGRhdGEtbmFt
  ZT0icGF0aDQgZmlsbCIgZD0iTTEzNy43MywzNS4xN2ExLjY1LDEuNjUsMCwwLDEtMS42NS0xLjc2
  LDEuNjgsMS42OCwwLDAsMSwxLjcxLTEuNzUsMS42NCwxLjY0LDAsMCwxLDEuNjgsMS43NSwxLjY2
  LDEuNjYsMCwwLDEtMS43MSwxLjc2WiIvPjwvZz48ZyBpZD0iRmlsbF8xMSIgZGF0YS1uYW1lPSJG
  aWxsIDExIj48cGF0aCBpZD0icGF0aDVfZmlsbCIgZGF0YS1uYW1lPSJwYXRoNSBmaWxsIiBkPSJN
  MTQzLDI1LjU5YzAtMS41OSwwLTMtLjExLTQuMjFIMTQ1bC4xMSwyLjY1aC4wOWE0LjE1LDQuMTUs
  MCwwLDEsMy44NC0zLDUuMzYsNS4zNiwwLDAsMSwuNjguMDV2Mi4zMWE3LjI0LDcuMjQsMCwwLDAt
  Ljg1LS4wNiwzLjQ4LDMuNDgsMCwwLDAtMy4zNiwzLjEyLDcuMzQsNy4zNCwwLDAsMC0uMDgsMS4x
  NHY3LjIySDE0M1oiLz48L2c+PGcgaWQ9Ikdyb3VwXzE1IiBkYXRhLW5hbWU9Ikdyb3VwIDE1Ij48
  ZyBjbGFzcz0iY2xzLTIiPjxnIGlkPSJGaWxsXzEzIiBkYXRhLW5hbWU9IkZpbGwgMTMiPjxwYXRo
  IGlkPSJwYXRoN19maWxsIiBkYXRhLW5hbWU9InBhdGg3IGZpbGwiIGQ9Ik0xNjQsMzEuMTZjMCwx
  LjQyLDAsMi42NS4xMSwzLjcxaC0yLjIybC0uMTQtMi4yaC0uMDZhNS4xNiw1LjE2LDAsMCwxLTQu
  NTUsMi41MWMtMi4xNiwwLTQuNzUtMS4yLTQuNzUtNS45MVYyMS4zOGgyLjUxdjcuNDRjMCwyLjU2
  LjgzLDQuMzIsMy4wNyw0LjMyYTMuNiwzLjYsMCwwLDAsMy4zLTIuMjYsMy4zLDMuMywwLDAsMCwu
  MjMtMS4yNVYyMS4zOEgxNjRaIi8+PC9nPjwvZz48L2c+PGcgaWQ9IkZpbGxfMTYiIGRhdGEtbmFt
  ZT0iRmlsbCAxNiI+PHBhdGggaWQ9InBhdGg4X2ZpbGwiIGRhdGEtbmFtZT0icGF0aDggZmlsbCIg
  ZD0iTTc1Ljc3LDMxLjYxVjI2LjczYzAtMy0xLjI4LTUuNzQtNS43Mi01Ljc0YTEwLjE0LDEwLjE0
  LDAsMCwwLTUsMS4ybC42OCwyLjIzYTcuNDcsNy40NywwLDAsMSwzLjczLTFjMi4yMiwwLDIuNzMs
  MS4yMiwyLjgxLDIuMTFhMjYsMjYsMCwwLDEsLjEsMy41NGMwLC4wNiwwLC4wOSwwLC4xNHYuOWEy
  LjI4LDIuMjgsMCwwLDEtLjA4Ljc1LDIuODcsMi44NywwLDAsMS0yLjc2LDEuODcsMS44MiwxLjgy
  LDAsMCwxLTItMmMwLTEuNzgsMS44My0yLjQsMy45MS0yLjUxbDEuMTYsMEw3Mi4zNSwyNmwtMSww
  QzY2LjgzLDI2LjIsNjQsMjcuOTQsNjQsMzEuMTNhNC4wNyw0LjA3LDAsMCwwLDQuMzUsNCw1LjEx
  LDUuMTEsMCwwLDAsNC4xLTEuODFoLjA4bC4yNiwxLjVINzZBMTgsMTgsMCwwLDEsNzUuNzcsMzEu
  NjFaIi8+PC9nPjxnIGlkPSJGaWxsXzE4IiBkYXRhLW5hbWU9IkZpbGwgMTgiPjxwYXRoIGlkPSJw
  YXRoOV9maWxsIiBkYXRhLW5hbWU9InBhdGg5IGZpbGwiIGQ9Ik0xMDUuMjQsMjcuNmMwLTMtMS40
  OC02LjYtNi02LjZzLTYuNzcsMy41NC02Ljc3LDcuM2MwLDQuMTUsMi42NSw2Ljg1LDcuMTQsNi44
  NWExMi42OCwxMi42OCwwLDAsMCw0Ljg0LS44NkwxMDQsMzEuOTRhMTEuMzksMTEuMzksMCwwLDEt
  My44NC42MSw0LjEzLDQuMTMsMCwwLDEtMy44OS0yQTIuNjgsMi42OCwwLDAsMSw5NiwzMGMwLS4w
  NSwwLS4xMSwwLS4xN2E5LjkyLDkuOTIsMCwwLDEtLjE4LTIuMSw3LjIsNy4yLDAsMCwxLC4xNi0x
  LjU2TDk2LDI2YTMuNTEsMy41MSwwLDAsMSwuMTItLjQyLDMuMTEsMy4xMSwwLDAsMSwzLTIuMjlj
  Mi4zMywwLDIuOSwyLjA2LDIuODgsMy4zMmgtNi4zVjI5aDkuNUE4LjI5LDguMjksMCwwLDAsMTA1
  LjI0LDI3LjZaIi8+PC9nPjxnIGlkPSJGaWxsXzIwIiBkYXRhLW5hbWU9IkZpbGwgMjAiPjxwYXRo
  IGlkPSJwYXRoMTBfZmlsbCIgZGF0YS1uYW1lPSJwYXRoMTAgZmlsbCIgZD0iTTEwOC40MywxNlYz
  NC44N2gzLjVWMTUuMDZaIi8+PC9nPjxnIGlkPSJGaWxsXzIyIiBkYXRhLW5hbWU9IkZpbGwgMjIi
  PjxwYXRoIGlkPSJwYXRoMTFfZmlsbCIgZGF0YS1uYW1lPSJwYXRoMTEgZmlsbCIgZD0iTTExNS44
  NiwxNlYzNC44N2gzLjVWMTUuMDdaIi8+PC9nPjwvZz48ZyBpZD0iR3JvdXBfMzQiIGRhdGEtbmFt
  ZT0iR3JvdXAgMzQiPjxnIGlkPSJHcm91cF8yNyIgZGF0YS1uYW1lPSJHcm91cCAyNyI+PGcgY2xh
  c3M9ImNscy0zIj48ZyBpZD0iRmlsbF8yNSIgZGF0YS1uYW1lPSJGaWxsIDI1Ij48cGF0aCBpZD0i
  cGF0aDEzX2ZpbGwiIGRhdGEtbmFtZT0icGF0aDEzIGZpbGwiIGNsYXNzPSJjbHMtNCIgZD0iTTIx
  LjYyLDQyLjNjMi41MS0xMS43OS0yLjc2LTEzLTExLjYzLTIzLjg3UzE1LjkuODYsMTcuMjcuNWMx
  LS4yNi00LjU5LS44Mi05Ljc1LDEuNjMtOC42Myw0LjEtNy40LDE2LTcuNCwxNlMtMS4wNywyMy40
  NSwzLjksMjguNWMzLjgzLDMuODksOCw2LjQ0LDkuMjMsMTQuMjRsNy4zMS40OFoiLz48L2c+PC9n
  PjwvZz48ZyBpZD0iR3JvdXBfMzAiIGRhdGEtbmFtZT0iR3JvdXAgMzAiPjxnIGNsYXNzPSJjbHMt
  NSI+PGcgaWQ9IkZpbGxfMjgiIGRhdGEtbmFtZT0iRmlsbCAyOCI+PHBhdGggaWQ9InBhdGgxNV9m
  aWxsIiBkYXRhLW5hbWU9InBhdGgxNSBmaWxsIiBjbGFzcz0iY2xzLTYiIGQ9Ik0yNS4xNiwzMy44
  MWM3LjYxLTkuMjctNS45My0xNy4zOS03LTI0LjIxczkuNDUtNy4yNSw5LjQ1LTcuMjVMMTksMGE1
  Ljk0LDUuOTQsMCwwLDEtMS45NS41Yy01LjcsMC0xNy43Miw4LjgyLTYuNDIsMjFDMTYsMjcuMjEs
  MTkuNTYsMzEuODIsMjEsNDAuMiwyMSw0MC4yLDIzLjczLDM1LjU2LDI1LjE2LDMzLjgxWiIvPjwv
  Zz48L2c+PC9nPjxnIGlkPSJHcm91cF8zMyIgZGF0YS1uYW1lPSJHcm91cCAzMyI+PGcgY2xhc3M9
  ImNscy03Ij48ZyBpZD0iRmlsbF8zMSIgZGF0YS1uYW1lPSJGaWxsIDMxIj48cGF0aCBpZD0icGF0
  aDE3X2ZpbGwiIGRhdGEtbmFtZT0icGF0aDE3IGZpbGwiIGNsYXNzPSJjbHMtOCIgZD0iTTI0LjUy
  LDM0LjU2QzMwLjg2LDIzLjI4LDE5LDE3LjI0LDE3Ljg4LDEwLjQyYTYuNjUsNi42NSwwLDAsMSw3
  LjU3LThjMS45NC40Nyw0LjY4LDEuNjQsNy40Miw1Ljg2LDQuODMsNy40NS0yLjgxLDIyLjI3LTIu
  ODEsMjIuMjdaIi8+PC9nPjwvZz48L2c+PC9nPjxnIGlkPSJGaWxsXzM1IiBkYXRhLW5hbWU9IkZp
  bGwgMzUiPjxwYXRoIGlkPSJwYXRoMThfZmlsbCIgZGF0YS1uYW1lPSJwYXRoMTggZmlsbCIgZD0i
  TTEzLjU3LDQzLjU1aDB2MS40OWEzLjg1LDMuODUsMCwwLDAsMy43LDRoMGEzLjg1LDMuODUsMCww
  LDAsMy43LTRWNDMuNTZIMTMuNTdaIi8+PC9nPjwvZz48L2c+PC9zdmc+
  " alt="Red dot"></span></td>
  													</tr>
  												</table>
  											</td>
  										</tr>
  									</table><div style="display: none;" id="element_02662967605574885"></div></p></div>

  									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
  										<tr>
  											<td width="352" valign="middle" align="center">

  												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
  													<tr>
  														<td width="100%" height="45"></td>
  													</tr>
  												</table>
  											</td>
  										</tr>
  									</table>

  									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
  										<tr>
  											<td width="352" valign="middle" align="center">

  												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
  													<tr>
  														<td width="265" bgcolor="#fdba30"class="pad15" style="background-color: rgb(237, 123, 65);">

  															<table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
  																<tr>
  																	<td width="100%" height="30"></td>
  																</tr>
  																<tr>
  																	<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 30px; color: rgb(255, 255, 255); line-height: 34px;"class="fullCenter"   cu-identify="element_034354677717032733"><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;">${req.body.name}, cпасибо, что подписались на нашу рассылку<!--[if !mso]><!--></span><!--<![endif]--></td>
  																</tr>
  															</table>
  														</td>
  													</tr>
  												</table>

  											</td>
  										</tr>
  									</table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255); position: relative; z-index: 0;">
  										<tr>
  											<td width="352" valign="middle" align="center">

  												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
  													<tr>
  														<td width="265" bgcolor="#fdba30"class="pad15" style="background-color: rgb(237, 123, 65);">

  															<table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
  																<tr>
  																	<td width="100%" height="30"></td>
  																</tr>
  															</table>
  														</td>
  													</tr>
  												</table>

  											</td>
  										</tr>
  									</table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255); position: relative; z-index: 0;">
  										<tr>
  											<td width="352" valign="middle" align="center">

  												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
  													<tr>
  														<td width="265" bgcolor="#fdba30"class="pad15" align="center" style="background-color: rgb(237, 123, 65);">

  															<table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

  																<tr>
  																	<td valign="middle" align="center" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(255, 255, 255); line-height: 24px;"class="fullCenter"   cu-identify="element_0318336642111021"><div style="text-align: left;"><span style="font-family: proxima_nova_rgregular, Helvetica;"><a href="http://travelly.ru">travelly.ru</a> - это путешествия в рассрочку. Решение по заявке принимается за 1 минуту в режиме online.</span></div><!--[if !mso]><!--><!--<![endif]--></td>
  																</tr>
  															</table>
  														</td>
  													</tr>
  												</table>

  											</td>
  										</tr>
  									</table>

  									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255); position: relative; z-index: 0;">
  										<tr>
  											<td width="352" valign="middle" align="center">

  												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
  													<tr>
  														<td width="265" bgcolor="#fdba30"class="pad15" style="background-color: rgb(237, 123, 65);">

  															<table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
  																<tr>
  																	<td width="100%" height="30"></td>
  																</tr>
  															</table>
  														</td>
  													</tr>
  												</table>

  											</td>
  										</tr>
  									</table>

  									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
  										<tr>
  											<td width="352" valign="middle" align="center">

  												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
  													<tr>
  														<td width="265" bgcolor="#fdba30"class="pad15" align="center" style="background-color: rgb(237, 123, 65);">

  															<table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

  																<tr>
  																	<td valign="middle" align="center" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(255, 255, 255); line-height: 24px;"class="fullCenter"   cu-identify="element_0003678592791917734"><div style="text-align: left;"><span style="font-family: proxima_nova_rgregular, Helvetica;"><a href="http://travelly.ru">travelly.ru</a> - собирает лучшие предложения по семейному и детскому отдыху, а удобный поиск помогает найти подходящий вариант, который не нанесет ущерб семейному бюджету.</span></div><!--[if !mso]><!--><!--<![endif]--></td>
  																</tr>
  															</table>
  														</td>
  													</tr>
  												</table>

  											</td>
  										</tr>
  									</table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
  										<tr>
  											<td width="352" valign="middle" align="center">

  												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
  													<tr>
  														<td width="265" bgcolor="#fdba30"class="pad15" align="center" style="background-color: rgb(237, 123, 65);">

  															<table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

  																<tr>
  																	<td valign="middle" align="center" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(255, 255, 255); line-height: 24px;"class="fullCenter"   cu-identify="element_004419303678484976"><div style="text-align: left;">Мы будем держать Вас в курсе наших новостей!</div><div style="text-align: left;">Если Вас интересуют партнерские программы или другие формы сотрудничества - напишите нам об этом на partner@travelly.ru</div><!--[if !mso]><!--><!--<![endif]--></td>
  																</tr>
  															</table>
  														</td>
  													</tr>
  												</table>

  											</td>
  										</tr>
  									</table>

  									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
  										<tr>
  											<td width="352" valign="middle" align="center">

  												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
  													<tr>
  														<td width="265" bgcolor="#fdba30"class="pad15" style="background-color: rgb(237, 123, 65);">

  															<table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
  																<tr>
  																	<td width="100%" height="40"></td>
  																</tr>
  															</table>
  														</td>
  													</tr>
  												</table>

  											</td>
  										</tr>
  									</table>
  									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
  										<tbody><tr>
  											<td width="352" valign="middle" align="center">

  												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
  													<tbody><tr>
  														<td width="265" bgcolor="#fdba30" class="pad15" align="center" style="background-color: rgb(237, 123, 65);">

  															<table width="240" border="0" cellpadding="0" cellspacing="0" align="center" class="full">

  																<tbody><tr>
  																	<td valign="middle" align="center" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(255, 255, 255); line-height: 24px;" class="fullCenter"><div style="text-align: left; margin-bottom:10px;">С уважением, Команда <a href="http://travelly.ru">travelly.ru</a></div><!--[if !mso]><!--><!--<![endif]--></td>
  																</tr>
  															</tbody></table>
  														</td>
  													</tr>
  												</tbody></table>

  											</td>
  										</tr>
  									</tbody>
  									</table>
  									<!----------------- Button Center ----------------->
  									<div style="display: none" id="element_025321369409377414"></div></p></div><!----------------- End Button Center ----------------->

  									<div style="display: none" id="element_06833590617705095"></div></p></div>

  									<div style="display: none" id="element_046486231700409797"></div></p></div>

  									<div style="display: none" id="element_07341349895821935"></div></p></div>

  									<div style="display: none" id="element_017400325347341206"></div></p></div>

  									<div style="display: none" id="element_04756115496404655"></div></p></div>

  									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff"style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; background-color: rgb(255, 255, 255);" object="drag-module-small">
  										<tr>
  											<td width="352" valign="middle" align="center">

  												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
  													<tr>
  														<td width="265" height="50"></td>
  													</tr>
  												</table>

  											</td>
  										</tr>
  									</table><!-- End Top -->
  									</div>

  								</td>
  							</tr>
  						</table>

  						<div class="sortable_inner ui-sortable">
  						<!-- Space -->
  						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
  							<tr>
  								<td width="352" height="30"></td>
  							</tr>
  						</table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small" style="position: relative; z-index: 0;">
  							<tr>
  								<td width="352" valign="middle" align="center">

  									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
  										<tr>
  											<td width="352" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #3f4345; line-height: 24px;"class="fullCenter" ><!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica;"><!--<![endif]--><!--subscribe--><a href="#" style="text-decoration: none; color: #3f4345;" cu-identify="element_06630269605028183"></a><!--unsub--><!--[if !mso]><!--></span><!--<![endif]-->
  											<span  cu-identify="element_033989418249416137"><span style="font-family: 'proxima_nova_rgregular', Helvetica;">Нажмите <a href="http://#">отписаться</a> если Вы не хотите получать новости проекта travelly.ru<!--[if !mso]><!--></span><!--<![endif]--></span>
  											</td>
  										</tr>
  									</table>

  								</td>
  							</tr>
  						</table><div style="display: none" id="element_05451050011910535"></div></p></div><!-- End Space -->

  						<!-- Space -->
  						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
  							<tr>
  								<td width="352" height="50"></td>
  							</tr>
  							<tr>
  								<td width="352" height="1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
  							</tr>
  						</table><!-- End Space -->
  						</div>

  					</td>
  				</tr>
  			</table>

  		</div>
  		</td>
  	</tr>
  </table><!-- End Notification 3 -->

  </div>
  </body></html>	<style>body{ background: none !important; } </style>
`;
};
