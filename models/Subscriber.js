var mongoose = require('mongoose')

var subscriberSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    default:null
  },
  email:{
    type: String,
    required: true,
    default:null
  },
  phone:{
    type: String,
    default:null
  },
  display:{
    type: String,
    default: "subscriber"
  }
})

module.exports = mongoose.model('Subscriber', subscriberSchema)
