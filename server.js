const express = require("express");
const path = require("path");
const dotenv           = require("dotenv");
const compression = require("compression");
const methodOverride = require("method-override");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const expressValidator = require("express-validator");
const Subscriber = require("./models/Subscriber");
const mongoose = require("mongoose");




// Load environment variables = require(' .env file
dotenv.load();

console.log(process.env.MONGODB_URI);
if (mongoose.connection.readyState) {
  setTimeout(function() {
    mongoose.connect(
      process.env.MONGODB_URI,
      { useMongoClient: true },
      (err, done) => {}
    );
  }, 100);
} else {
  mongoose.connect(
    process.env.MONGODB_URI,
    { useMongoClient: true },
    (err, done) => {}
  );
}

const app = express();

var nodemailer = require("nodemailer");
var transporter = nodemailer.createTransport({
  service: "Mailgun",
  auth: {
    user: "postmaster@mailing.travelly.ru",
    pass: "d26a6a1829f52619fdbaff4f3bff8161"
  }
});

app.set("port", process.env.PORT || 3000);
app.use(compression());
app.use(bodyParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(methodOverride("_method"));
app.use(express.static(path.join(__dirname, "public")));

app.get("/", async (req, res) => {
  res.sendFile(path.join(__dirname, "./src", "index.html"));
});

app.post("/subscribe", async (req, res, next) => {
  req.assert("name", "Name cannot be blank").notEmpty();
  req.assert("email", "Email is not valid").isEmail();
  req.assert("email", "Email cannot be blank").notEmpty();
  req.sanitize("email").normalizeEmail({ remove_dots: false });

  let errors = req.validationErrors();

  if (errors) {
    return res.json(errors);
  }




  Subscriber.findOne(
    {
      email: req.body.email
    },
    (err, subscriber) => {
      if (subscriber) {
        let errors = {
          msg: `The email ${req.body.email} address you have entered is already associated with another subscriber.`
        };
        return next(errors);
      }
      else{
        const newSubscriber = new Subscriber({
          name: req.body.name,
          email: req.body.email
        })
        const mailOptions = {
          from: req.body.name + " " + "<" + req.body.email + ">",
          to: "info@travelly.ru",
          subject: "✔ Contact Form",
          text: `New subscriber ${req.body.name} with ${req.body.email}`
        };
        transporter.sendMail(mailOptions, function(err) {
          console.log(mailOptions);
        });
        const html = require('./src/subscribe')(req);
        console.log(html);
        const subscriberOptions = {
          from: "Travelly.ru " + "<info@travelly.ru>",
          to: req.body.email,
          subject: "✔ Contact Form",
          text: `Thx from Travelly Team ${req.body.name}`,
          html: html
        };
        transporter.sendMail(subscriberOptions, function(err) {
          res.json({ msg: "Thank you! Your feedback has been submitted." });
          newSubscriber.save();
        });
      }
    }
  );

});


app.post("/partner", async (req, res, next) => {
  req.assert("name", "Name cannot be blank").notEmpty();
  req.assert("email", "Email is not valid").isEmail();
  req.assert("email", "Email cannot be blank").notEmpty();
  req.sanitize("email").normalizeEmail({ remove_dots: false });

  let errors = req.validationErrors();

  if (errors) {
    return res.json(errors);
  }




  Subscriber.findOne(
    {
      email: req.body.email
    },
    (err, subscriber) => {
      if (subscriber) {
        let errors = {
          msg: `The email ${req.body.email} address you have entered is already associated with another subscriber.`
        };
        return next(errors);
      }
      else{
        const newSubscriber = new Subscriber({
          name: req.body.name,
          email: req.body.email,
          phone: req.body.tel,
          display: "partner"
        })
        const mailOptions = {
          from: req.body.name + " " + "<" + req.body.email + ">",
          to: "info@travelly.ru",
          subject: "✔ Contact Form",
          text: `New subscriber ${req.body.name} with ${req.body.email}`
        };
        transporter.sendMail(mailOptions, function(err) {
          console.log(mailOptions);
        });
        const html = require('./src/subscribe')(req);
        console.log(html);
        const subscriberOptions = {
          from: "Travelly.ru " + "<info@travelly.ru>",
          to: req.body.email,
          subject: "✔ Contact Form",
          text: `Thx from Travelly Team ${req.body.name}`,
          html: html
        };
        transporter.sendMail(subscriberOptions, function(err) {
          res.json({ msg: "Thank you! Your feedback has been submitted." });
          newSubscriber.save();
        });
      }
    }
  );

});



// Production error handler
if (app.get("env") === "production") {
  app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.sendStatus(err.status || 404);
  });
}
//
app.listen(app.get("port"), function() {
  console.log("Express server listening on port " + app.get("port"));
});

module.exports = app;
