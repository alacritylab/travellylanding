const gulp         = require('gulp'),
      plumber      = require('gulp-plumber'),
      rename       = require('gulp-rename'),
      autoprefixer = require('gulp-autoprefixer'),
      babel        = require('gulp-babel'),
      concat       = require('gulp-concat'),
      jshint       = require('gulp-jshint'),
      uglify       = require('gulp-uglify'),
      imagemin     = require('gulp-imagemin'),
      cache        = require('gulp-cache'),
      minifycss    = require('gulp-minify-css'),
      sass         = require('gulp-sass'),
      pug          = require('gulp-pug'),
      browserSync  = require('browser-sync');

gulp.task('browser-sync', function() {
  browserSync({
    server: {
       baseDir: "./public"
    }
  });
});

gulp.task('bs-reload', function () {
  browserSync.reload();
});

gulp.task('images', function(){
  gulp.src('src/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('public/assets/img/'));
});

gulp.task('styles', function(){
  gulp.src(['src/scss/*.*'])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(sass())
    .pipe(autoprefixer('last 2 versions'))
    .pipe(gulp.dest('public/assets/css/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('public/assets/css/'))
    .pipe(browserSync.reload({stream:true}));
  gulp.src(['src/css/*.*'])
       .pipe(gulp.dest('public/assets/css/'))
});

gulp.task('scripts', function(){
  return gulp.src('src/scripts/**/*.js')
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(babel())
    .pipe(gulp.dest('public/assets/js/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('public/assets/js/'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('default', ['browser-sync'], function(){
  gulp.watch("src/scss/**/*.scss", ['styles']);
  gulp.watch("src/scripts/**/*.js", ['scripts']);
  gulp.watch("*.html", ['bs-reload']);
});
gulp.task('build', ['styles','images','scripts']);
