(function ($) {
  $.fn.countdown = function (options, callback) {
    //custom 'this' selector
    thisEl = $(this);

    // array of custom settings
    var settings = {
      date: null,
      format: null
    };

    // append the settings array to options
    if (options) {
      $.extend(settings, options);
    }

    //create the countdown processing function
    function countdown_proc() {
      var eventDate = Date.parse(settings.date) / 1000;
      var currentDate = Math.floor($.now() / 1000);

      if (eventDate <= currentDate) {
        callback.call(this);
        clearInterval(interval);
      }

      var seconds = eventDate - currentDate;

      var days = Math.floor(seconds / (60 * 60 * 24));
      //calculate the number of days

      seconds -= days * 60 * 60 * 24;
      //update the seconds variable with number of days removed

      var hours = Math.floor(seconds / (60 * 60));
      seconds -= hours * 60 * 60;
      //update the seconds variable with number of hours removed

      var minutes = Math.floor(seconds / 60);
      seconds -= minutes * 60;
      //update the seconds variable with number of minutes removed

      //conditional statements
      if (days % 10 == 1) {
        thisEl.find(".timeRefDays").text("ДЕНЬ");
      } else if (days % 10 == 2 || days % 10 == 3 || days % 10 == 4) {
        thisEl.find(".timeRefDays").text("ДНЯ");
      } else {
        thisEl.find(".timeRefDays").text("ДНЕЙ");
      }
      if (hours % 10 == 1) {
        thisEl.find(".timeRefHours").text("ЧАС");
      } else if (hours % 10 == 2 || hours % 10 == 3 || hours % 10 == 4) {
        thisEl.find(".timeRefHours").text("ЧАСА");
      } else {
        thisEl.find(".timeRefHours").text("ЧАСОВ");
      }
      if (minutes % 10 == 1) {
        thisEl.find(".timeRefMinutes").text("МИНУТА");
      } else if (minutes % 10 == 2 || minutes % 10 == 3 || minutes % 10 == 4) {
        thisEl.find(".timeRefMinutes").text("МИНУТЫ");
      } else {
        thisEl.find(".timeRefMinutes").text("МИНУТ");
      }
      //alert(seconds%10);

      if (seconds % 10 == 1) {
        thisEl.find(".timeRefSeconds").text("СЕКУНДА");
        //break;
      } else if (seconds % 10 == 2 || seconds % 10 == 3 || seconds % 10 == 4) {
        thisEl.find(".timeRefSeconds").text("СЕКУНДЫ");
      } else {
        thisEl.find(".timeRefSeconds").text("СЕКУНД");
      }

      //logic for the two_digits ON setting
      if (settings.format == "on") {
        days = String(days).length >= 2 ? days : "0" + days;
        hours = String(hours).length >= 2 ? hours : "0" + hours;
        minutes = String(minutes).length >= 2 ? minutes : "0" + minutes;
        seconds = String(seconds).length >= 2 ? seconds : "0" + seconds;
      }

      //update the countdown's html values.
      thisEl.find(".days").text(days);
      thisEl.find(".hours").text(hours);
      thisEl.find(".minutes").text(minutes);
      thisEl.find(".seconds").text(seconds);
    }

    //run the function
    countdown_proc();

    //loop the function
    interval = setInterval(countdown_proc, 1000);
  };
})(jQuery);

//Provide the plugin settings
$("#countdown").countdown({
  //The countdown end date
  date: "20 September 2017 00:00:00",

  // on (03:07:52) | off (3:7:52) - two_digits set to ON maintains layout consistency
  format: "on"
}, function () {
  // This will run when the countdown ends
  alert("We're Out Now");
});

document.addEventListener("DOMContentLoaded", function (event) {
  var discoverName = document.querySelector(".discover-name"),
      discoverText = document.querySelector(".discover-mail"),
      discoverSubmit = document.querySelector(".js-form-submit"),
      partnerSubmit = document.querySelector(".js-form-submit-another"),
      regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  function validate(name) {
    var statusA = -1,
        statusB = -2;
    if (discoverName.value === "" || discoverText.value === "" && regEx.test(discoverText.value)) {
      if (discoverName.value === "") discoverName.classList.add("danger");
      statusB = -2;
    } else {
      discoverName.classList.remove("danger");
      statusB = 1;
    }
    if (discoverText.value === "" || !regEx.test(discoverText.value)) {
      discoverText.classList.add("danger");
      statusA = -1;
    } else if (regEx.test(discoverText.value)) {
      discoverText.classList.remove("danger");
      statusA = 1;
    } else {
      statusA = 1;
      discoverText.classList.remove("danger");
    }

    if (statusA === statusB) {
      discoverSubmit.disabled = false;
      discoverSubmit.classList.add("true-btn");
    } else {
      discoverSubmit.classList.remove("true-btn");
    }
  }

  function PopUp(className, msg) {
    var _this = this;
    this.className = className;
    this.msg = msg;
    this.popUp = $('<div class="popUpMsg ' + this.className + '"><p class="popup-text">' + this.msg + '</p><input class="form-submit popup-btn js-close" type="button" value="Хорошо"></div>');
    this.open = function () {
      this.popUp.appendTo("body");
      $("." + _this.className).fadeIn(500);
      $(".shadow").fadeIn(500);
    };
    this.close = function () {
      $("." + _this.className).fadeOut(500);
      $(".shadow").fadeOut(500);
    };
    this._addEventListeners = function () {
      $(document).on("click", ".shadow", function (e) {
        _this.close();
      });
      $(document).on("click", ".js-close", function (e) {
        _this.close();
      });
    };
  }

  var successPopUp = new PopUp("js-success", "Спасибо, мы скоро свяжемся с вами"),
      errorPopUp = new PopUp("js-error", "Извините что-то пошло не так, попробуйте позже");

  function getAjax(where) {
    console.log(where);
    $.ajax({
      url: "/" + where + "",
      type: "POST",
      data: {
          name: where === "subscribe"? discoverName.value: $(".discover-name-partn").val(),
          email: where === "subscribe" ? discoverText.value : $(".discover-mail-partn").val(),
          tel: where === "subscribe" ? "" : $(".discover-tel-partn").val()
      },
      success: function (data) {
        successPopUp.open();
        successPopUp._addEventListeners();
      },
      error: function (e) {
        errorPopUp.open();
        errorPopUp._addEventListeners();
      }
    });
  }
  discoverName.addEventListener("keydown", function (e) {
    validate();
  });
  discoverName.addEventListener("change", function (e) {
    validate();
  });
  discoverText.addEventListener("keydown", function (e) {
    validate();
  });
  discoverText.addEventListener("change", function (e) {
    validate();
  });

  discoverSubmit.addEventListener("click", function (e) {
    getAjax("subscribe");
    e.preventDefault();
  });
  partnerSubmit.addEventListener("click", function (e) {
    getAjax("partner");
    e.preventDefault();
  });
  $(".js-slide-box").on("click", function (e) {
    if ($(e.target).hasClass("js-partners-click")) {
      $(".discover-form").slideDown();
      $(".discover-text").slideDown();
      $(".discover-form-two").slideUp();
      $(e.target).removeClass("js-partners-click");
      $(".discover-partners svg").removeClass("rotate");
    } else {
      $(".discover-form").slideUp();
      $(".discover-text").slideUp();
      $(".discover-form-two").slideDown();
      $(e.target).addClass("js-partners-click");
      $(".discover-partners svg").addClass("rotate");
    }
  });
});