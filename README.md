### Configuration
- **Platform:** node
- **Framework**: express
- **Template Engine**: none
- **CSS Framework**: none
- **CSS Preprocessor**: sass
- **JavaScript Framework**: 
- **Build Tool**: 
- **Unit Testing**: none
- **Database**: none
- **Authentication**: 
- **Deployment**: none

### How to run

First, run `npm` command to install all dependencies.
```
npm install
```

Then, run `gulp` command to start server and compile jade and sass files.
```
gulp
```

### Features

1. Crossbrowser testing by using BrowserSync.
2. Handle errors and notify it through System messages.
3. Sass file with comman mixins(with usage) and variables.
4. Jade templates with partials and data json.

### This project uses.



|№| Gulp Plugin | description |
|-| ----------- | ----------- |
|1.|[browser-sync](https://github.com/browsersync/browser-sync)| lauches the server for crossbrowser testing.|
|2.|[gulp-autoprefixer](http://github.com)||
|3.|[gulp-babel](http://github.com)||
|4.|[gulp-cache](http://github.com)|A temp file based caching proxy task for gulp.|
|5.|[gulp-concat](http://github.com)||
|6.|[gulp-imagemin](http://github.com)|Minify PNG, JPEG, GIF and SVG images|
|7.|[gulp-jshint](http://github.com)|to check for errors and warnings|
|8.|[gulp-minify-css](http://github.com)||
|9.|[gulp-notify](https://github.com/mikaelbr/gulp-notify)| notification plugin for gulp.|
|10.|[gulp-plumber](https://github.com/floatdrop/gulp-plumber)| Prevent pipe breaking caused by errors from gulp plugins.|
|11.|[gulp-pug](https://github.com/phated/gulp-jade)| compile your jade files.|
|12.|[gulp-rename](http://github.com)| rename files easily|
|13.|[gulp-sass](https://github.com/dlmanning/gulp-sass)| compile your sass files.|
|15.|[gulp-uglify](http://github.com)||





Using Gulp
==========

Default Task
------------

    gulp

Running the default task automatically watches your project folders for any changes and runs the accompanying task.

For example, if you've elected to run tasks on your  frontend-JavaScript, anytime you change a JavaScript file gulp will automatically run those tasks, including a browser refresh if you've included BrowserSync.

Build
---
    gulp build

Running the ```gulp [styles,images]```   tasks will run once.



CSS
---
    gulp styles

Running the gulp styles task will run your selected CSS tasks once.

JavaScript
----------

    gulp scripts

Running the gulp scripts task will run your selected JavaScript tasks once.

Images
------

    gulp images

Running the gulp images task will run your selected image tasks once.
